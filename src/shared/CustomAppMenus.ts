import { AppConsts } from '@shared/AppConsts';
import { Menu } from '@delon/theme';

export class CustomAppMenus {
  static Menus: Menu[] = [
    {// 工作台
      text: '',
      i18n: 'Test',
      acl:  undefined,
      icon: 'anticon anticon-dashboard',
      link: '/app/custom/test',
    }
  ];
}
