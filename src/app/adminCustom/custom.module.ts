import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { SharedModule } from '@shared/shared.module';
import { AbpModule } from '@abp/abp.module';

import { CustomRoutingModule } from '@app/adminCustom/custom-routing.module';
import { TesComponent } from './tes/tes';

@NgModule({
  imports: [
    FormsModule,
    CommonModule,
    HttpClientModule,
    SharedModule,
    AbpModule,
    CustomRoutingModule,
  ],
  declarations: [
    TesComponent
  ],
  entryComponents: [
  ],
  providers: [

  ],
})
export class CustomModule { }
