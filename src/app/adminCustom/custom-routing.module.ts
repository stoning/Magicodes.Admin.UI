import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TesComponent } from './tes/tes';

const routes: Routes = [
  { path: 'test', component: TesComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CustomRoutingModule { }
