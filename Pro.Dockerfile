# STEP 1: Build
FROM node:8-alpine as builder

COPY package.json package-lock.json ./

RUN npm config set registry https://registry.npm.taobao.org && npm set progress=false && npm config set depth 0 && npm cache clean --force
RUN npm i && mkdir /ng-app && cp -R ./node_modules ./ng-app

WORKDIR /ng-app

COPY . .
#内存溢出
#RUN npm run fix-memory-limit
RUN npm run publish-pro

# STEP 2: Setup
FROM nginx:1.13.5-alpine

COPY --from=builder /ng-app/_nginx/default.conf /etc/nginx/conf.d/

RUN rm -rf /usr/share/nginx/html/*

COPY --from=builder /ng-app/dist /usr/share/nginx/html

CMD [ "nginx", "-g", "daemon off;"]
